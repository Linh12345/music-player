package application;



import java.io.File;

import java.net.URL;
import java.util.ResourceBundle;

//import javax.sound.sampled.AudioFormat;
//import javax.sound.sampled.AudioInputStream;
//import javax.sound.sampled.AudioSystem;
//import javax.sound.sampled.DataLine;
//import javax.sound.sampled.SourceDataLine;
import javafx.scene.media.AudioClip;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.scene.control.Slider; //1
import javafx.event.EventHandler; //2
import javafx.beans.InvalidationListener;//3
import javafx.beans.value.ObservableValue;//4
import javafx.scene.input.MouseEvent; //5
import javafx.util.Duration; //6
import javafx.beans.Observable; //7
import javafx.beans.value.ChangeListener; //8
import javafx.scene.control.Label; //9
import javafx.beans.binding.Bindings; //10
import javafx.scene.media.MediaPlayer.Status;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
public class ViewController  implements Initializable  {
	File[] listFile ;
	int i = 0;
	File file1;
	int lengthListFile;
	String number = ""; // tao bien String de chua id cua button 
	int numberInt = 0;  // bien chua id cua button kieu int
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	}
	private Duration duration;
	@FXML
	private MediaPlayer mediaPlayer;
	@FXML
	private MediaView mediaShow;
	private String filePath;
	@FXML
	private Button Library;
	@FXML
	private Button Stop;
	@FXML
	private Button Prev;
	@FXML
	private Button Play;
	@FXML
	private Button Next;
	@FXML
	private Button repeat;
	@FXML
	private Button Folder;
	@FXML
	private Button Songs;
	@FXML
	private ScrollPane scrollPane;
	@FXML
	private Slider slider;
	@FXML
	private Slider seeSlider;
	@FXML
	private Label playTime;
	@FXML
	
	private void handlerButtonAction(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		FileChooser.ExtensionFilter filter= new FileChooser.ExtensionFilter("select file(*.mp3);(*.mp4);(*.wav);(*.wma)", "*.mp3","*.mp4","*.wav","*.wma" );
			fileChooser.getExtensionFilters().add(filter);
			File file = fileChooser.showOpenDialog(null);

			if(file != null)
			{
				filePath = file.toURI().toString();
				Media media = new Media(filePath);
				mediaPlayer = new MediaPlayer(media);
				mediaShow.setMediaPlayer(mediaPlayer);
				mediaShow.setX(400);
				mediaShow.setX(400);
				mediaShow.setY(350);
				updateValues(); 	

			}
			else
			{
				System.out.println("nothing");
			}
			
			//TODO
			// filePath = file.toURI().toString();
			// if(filePath != null) {
			// 	Media media = new Media(filePath);
			// 	mediaPlayer = new MediaPlayer(media);
			// 	mediaShow.setMediaPlayer(mediaPlayer);
			// 	mediaShow.setX(400);
			// 	mediaShow.setY(350);
			// 	//mediaPlayer.play();
			//  updateValues(); 	
			// }
			// else
			// {
			// 	System.out.println("nothing");
			// }
	}
	 @FXML
	 private void playButton(ActionEvent event) {
		 Status status = mediaPlayer.getStatus();

		    if (status == Status.PLAYING)
		    {
		        mediaPlayer.pause();
		        Image image = new Image(this.getClass().getResourceAsStream("/image/play.png"));
		        Play.setGraphic(new ImageView(image));
		    }
		    
		    if (status == Status.PAUSED
		     || status == Status.STOPPED
		     || status == Status.READY)
		    {
		        mediaPlayer.play();
		        Image image = new Image(this.getClass().getResourceAsStream("/image/pause.png"));
		        Play.setGraphic(new ImageView(image));
		    }		    		 
	 }
	 @FXML
	 private void stopButton(ActionEvent event) {
		 mediaPlayer.stop();
	 }
	 @FXML
	 private void chooseFolder(ActionEvent event) {
		 DirectoryChooser directoryChooser = new DirectoryChooser();
		 File dir = directoryChooser.showDialog(null);
		 VBox re = new VBox();
		 scrollPane.setContent(re);
		 re.setSpacing(10);
		 if (dir != null) {
			 listFile = dir.listFiles();
			 lengthListFile = listFile.length; // lay chieu dai cua danh sach bai hat
			 Media media = new Media(listFile[0].toURI().toASCIIString()); // lấy path của file đầu tiên làm mặc định
			 mediaPlayer = new MediaPlayer(media);
			 mediaShow.setMediaPlayer(mediaPlayer);
			 for(File file: listFile) {
				 Button button = new Button(); //tạo nút mới
        		 button.setText(file.getName()); // đặt tên nút
        		// button.setId(file.getPath()); // đặt ID cho nút, lấy id bằng path luôn
        		 button.setId(Integer.toString(i));
        		 button.setOnAction(click->{   // tạo một event trong nút
         		 mediaPlayer.stop(); // dừng cái đang phát hiện tại
         		 mediaPlayer = new MediaPlayer(new Media(file.toURI().toASCIIString())); //đổi thành media mới
         		 updateValues();
         		 mediaPlayer.play();
      			 number = button.getId();// lay id cua button 
      			 numberInt = Integer.parseInt(number);
         		 });
        		 re.getChildren().add(button); // tạo nút xong xuôi thì thêm vào VBOX -> quay lại dòng 90
        		 i = i+1;
			 }
			 
	    	
	}
         	
          else {
             System.out.println("nothing");
         }
     }
	 @FXML
	 private void showSong(ActionEvent event) {
		 System.out.println("you click me");
		 scrollPane.setVisible(true);
	 }
	 @FXML
	 private void playNext(ActionEvent event) {
		if(numberInt !=(lengthListFile-1) ) { // kiem tra id button cung tuc la thu tu cua bai hat co nam o cuoi cung hay khong
		numberInt = numberInt +1; // neu bai khong nam o cuoi thi so thu tu bai hat cong them 1
		mediaPlayer.stop(); // dung bai hat hien tai
		mediaPlayer = new MediaPlayer(new Media(listFile[numberInt].toURI().toString()));
		updateValues();
		mediaPlayer.play();// play bai ke tiep
		}else {
			numberInt = 0; // neu thu tu bai hat la cuoi cung thi gan id button bang khong de play bai hat dau tien
			mediaPlayer.stop();
			mediaPlayer = new MediaPlayer(new Media(listFile[numberInt].toURI().toString()));
			updateValues();
			mediaPlayer.play();// play bai hat dau tien trong danh sach
			
		}
		
	
	 }
	 @FXML
	 private void playPrev(ActionEvent event) {
		 if(numberInt != 0) {// kiem tra id button cung tuc la thu tu bai hat co phai la bai hat dau tien trong danh sach khong 
		 numberInt = numberInt -1;// neu khong phai thi so thu tu bai hat tru di 1
		 mediaPlayer.stop();// dung bai hat hien tai
		 mediaPlayer = new MediaPlayer(new Media(listFile[numberInt].toURI().toString()));
		 updateValues();
		 mediaPlayer.play();// play bai hat da phat luc truoc
		 }else {
			 numberInt = lengthListFile-1;// neu thu tu bai hat la dau tien thi so thu tu bai hat se bang cheu dai danh sach bai hat tru di 1
			 mediaPlayer.stop();
			 mediaPlayer = new MediaPlayer(new Media(listFile[numberInt].toURI().toString()));
			 updateValues();
			 mediaPlayer.play();// play bai hat cuoi cung
		 }
		 
	 }
	 protected void updateValues() { //ham de update lai thoi luong bai hat.
		slider.setValue(mediaPlayer.getVolume()*100); //thanh am luong
     	slider.valueProperty().addListener(new InvalidationListener()
     	{
     	@Override
     	public void invalidated(Observable observable)
     	{
     		mediaPlayer.setVolume(slider.getValue()/100); //gia tri am luong la 100
     	}
 });
      	
     	
     	mediaPlayer.currentTimeProperty().addListener(new ChangeListener<Duration>()
     			{
     		@Override
             public void changed(ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue)
             {
     			seeSlider.setValue(newValue.toSeconds());
     			seeSlider.maxProperty().bind(Bindings.createDoubleBinding(() -> mediaPlayer.getTotalDuration().toSeconds(),mediaPlayer.totalDurationProperty())); //tao gia tri cho thanh slider la thoi luong cua bai hat.
             }
     		
     			});
     	
     	seeSlider.setOnMouseClicked(new EventHandler<MouseEvent>()
     			{
     		 @Override
              public void handle(MouseEvent event)
              {
     			 mediaPlayer.seek(Duration.seconds(seeSlider.getValue())); //nhan chuot de chon toi thoi gian muon nghe
              }
     			});
     	seeSlider.setOnMouseDragged(new EventHandler<MouseEvent>()
     	{
     		 @Override
              public void handle(MouseEvent event)
              {
     			 mediaPlayer.seek(Duration.seconds(seeSlider.getValue())); //keo chuot de tua nhanh
              }
     			}
     			);
 	
		    }
}

